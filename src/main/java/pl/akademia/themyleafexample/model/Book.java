package pl.akademia.themyleafexample.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Book {
    private String author;
    private String title;
    private double price;

    private static List<Book> bookList = new ArrayList<>();

    public static List<Book> getBookList() {
        return bookList;
    }


static{
    for (int i = 1; i <=6 ; i++) {
        bookList.add(new Book("Author" + i, "Książka" + i, 49.99+10*i));
    }}

    @Override
    public String toString() {
        return getTitle()+" - " + getAuthor() + " (" + getStringPrice() + ")";
    }

    public String getStringPrice(){
        return String.format("%.2f", getPrice());
    }
}
