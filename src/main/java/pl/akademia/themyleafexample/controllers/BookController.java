package pl.akademia.themyleafexample.controllers;


import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import pl.akademia.themyleafexample.model.Book;

@Controller
public class BookController {

    @GetMapping("/bookadd")
    public String addBook(@RequestParam(required = false) String author
            , @RequestParam(required = false) String title,
                          @RequestParam(required = false) Double price
            , ModelMap map){


        Book book;
        if ( author!=null ) {
            book = new Book(author, title, price);
        }
        else{
            book = new Book("Author",
                    "Title",1.0);
        }
        map.put("book",book);
        return "add";
    }


    @GetMapping("/loop")
    public String listBooks(ModelMap map){
        map.put("books", Book.getBookList());
        return "booklist";
    }

}