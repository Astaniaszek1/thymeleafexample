package pl.akademia.themyleafexample.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import pl.akademia.themyleafexample.model.Article;


@Controller
public class ArticleController  {



        @GetMapping("")
    public  String   form() {
        return "article/form";

    }

    @PostMapping("/add")
    public String result(
            @ModelAttribute Article article, ModelMap modelMap){
        modelMap.put("article", article);
            return "article/formnew";
    }


}
