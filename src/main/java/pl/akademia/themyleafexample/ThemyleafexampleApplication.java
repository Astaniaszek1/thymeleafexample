package pl.akademia.themyleafexample;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ThemyleafexampleApplication {

    public static void main(String[] args) {

        SpringApplication.run(ThemyleafexampleApplication.class, args);
    }
}
